# Learning Progress Summary - Day 1

## GitLab:

### Understanding Git:

- **What is Git?**
  - Git is a version control system.
  - It allows many developers to work on the same project without being on the same network.

- **Git Concepts:**
  - Code history.
  - Takes "snapshots."
  - Decides when to take a snapshot through commits.

### Basic Commands:

- `init`, `add`, `status`, `commit`, `push`, `pull`, `clone`.

### Practice:

1. I created a folder on my local machine (`learningProgram`).
   - Added the file "index.html" using the `$touch ...` command.
   - Initialized the repo as a Git repo with `$ git init`.
   - Added the file and committed.

2. Learned to use `.gitignore` to ignore files or directories.

3. Branches:
   - Created a branch with `git branch`.
   - Switched to another branch with `git checkout`.

4. Worked with a remote repo on GitHub.

5. Learned how to clone a project to my local machine.

link : https://github.com/KHOURSSAfz/myAppSample

## JUnit:
    - a unit-testing fraework
    - support java 8 and above

### MAVEN DEPENDENCIES: (pom.xml)
    
    <dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-engine</artifactId>
    <version>5.9.2</version>
    <scope>test</scope>
    </dependency>

### Architecture: three main sub-projects:

     - JUnit Platform : lunch test framworks on the jvm
	 - JUnit Jupiter: new annotation
 	 - JUnit Vintage: provide smooth transition to jUnit 5

### Basic Annotations:

     - before the tests : @BeforeAll (!!! the methode should be static) and @BeforeEach
	 - during the tests : @DisplayName and @Disabled
	 - after the tests : @AfterEach and @AfterAll (static !!!)

###  I haven't fully understood JUnit 5 yet.
